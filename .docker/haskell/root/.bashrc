#!/bin/bash

# Write history one-by-one
export PROMPT_COMMAND='history -a;history -c;history -r'

# Haskell autocomplete
eval "$(stack --bash-completion-script stack)"
